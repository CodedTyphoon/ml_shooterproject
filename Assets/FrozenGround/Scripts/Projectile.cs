﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class Projectile : Recycleable
{
    public override RecycleableType RecycleableType { get { return RecycleableType.Ammo; } }
    public float maxTravelDistance = 50;
    public float speed = 25;
    public float damage = 30;
    public GameObject DamageImpact;
    public UnityEvent OnContact;
    public System.Action<bool> OnDamage; 
    bool isInitialized = false;
    float initializationTime;
    // Start is called before the first frame update
    void Start()
    {
        Initialize();
    }

    // Update is called once per frame
    void Update()
    {
        if (isInitialized)
        {
            if(Time.time > initializationTime + 2f)
            {
                isInitialized = false;
                Deactivate();
                return;
            }
            float distanceDelta = speed * Time.deltaTime;
            RaycastHit raycastHit;
            if (Physics.Raycast(transform.position, transform.forward, out raycastHit, distanceDelta))
            {
                float moveDistance = raycastHit.distance * Time.deltaTime;
                Vector3 location = transform.position + moveDistance * transform.forward;

                IDamageable damageable = raycastHit.transform.GetComponent<IDamageable>();
                if (damageable != null)
                {
                    bool dead = damageable.Damage(damage, location);
                    OnDamage?.Invoke(dead);
                }
                Contact(location);
            }
            else
            {
                transform.position += transform.forward * distanceDelta;
            }
        }
    }
    public void Contact(Vector3 location)
    {
        OnContact?.Invoke();
        transform.position = location;
        if(DamageImpact) DamageImpact.SetActive(true);
        isInitialized = false;
        Deactivate();
    } 
    public void Initialize()
    {
        isInitialized = true;
        initializationTime = Time.time;
    }
    private void OnDisable()
    {
        OnDamage = null;
    }
}
public interface IDamageable
{
    float CurrentHealth { get; set; }
    bool Damage(float damage, Vector3 location);
}