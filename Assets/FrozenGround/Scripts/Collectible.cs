﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Collectible : Recycleable, ICollectible
{
    public int CollectibleValue = 30;
    public CollectibleType CollectibleType { get { return collectibleType; } }
    public CollectibleType collectibleType;
    public override RecycleableType RecycleableType { get { return RecycleableType.Collectible; } }

    public int Collect()
    {
        return CollectibleValue;
    }
}
