﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
public enum RecycleableType { Ammo, VFX, Collectible }
public abstract class Recycleable: MonoBehaviour, IRecycleable<Recycleable>
{
    public abstract RecycleableType RecycleableType { get; }
    public System.Action<Recycleable> OnRecycle { get; set; }
    public virtual void Deactivate()
    {
        gameObject.SetActive(false);
        OnRecycle?.Invoke(this);
    }
}

public interface IRecycleable<T> 
{
    System.Action<T> OnRecycle { get; set; }
}