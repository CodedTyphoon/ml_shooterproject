﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
[ExecuteInEditMode]
public class ItemSpawner : MonoBehaviour
{
    public List<Transform> weaponLocations, healthLocations, boosterLocations, ammoLocations;
    public Transform weaponHolder, ammoHolder, healthHolder, boosterHolder;
    public GameObject weapon, ammo, health, booster;
    public bool execute;
    private void Update()
    {
        if (execute)
        {
            execute = false;
            Spawn();
        }
        
    }
    void Spawn()
    {
        Spawn(weaponLocations, weapon, weaponHolder);
        Spawn(healthLocations, health, healthHolder);
        Spawn(boosterLocations, booster, boosterHolder);
        Spawn(ammoLocations, ammo, ammoHolder);
    }
    void Spawn(List<Transform> locations, GameObject prefab, Transform parent)
    {
        foreach (var item in locations)
        {
            GameObject.Instantiate(prefab, parent).transform.position = item.position;
        }
    }
}
