﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum CollectibleType { Ammo, Health, Booster, Weapon}
public interface ICollectible
{
    CollectibleType CollectibleType { get; }
    int Collect();
}