﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class Extensions
{
    public static void MatchTransform(this Transform transform,Transform target, bool scale = false)
    {
        transform.position = target.position;
        transform.rotation = target.rotation;
        if(scale) transform.localScale = target.localScale;
    }
}
