﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CollectionTrainner : MonoBehaviour
{
    [SerializeField]
    private GameObject ammo, booster, health, dummy;
    // Start is called before the first frame update
    public void ResetTrainer()
    {
        ammo.SetActive(true);
        dummy.SetActive(false);
        booster.SetActive(false);
        health.SetActive(false);

        int randomValue = Random.Range(1, 91)/30;
        switch (randomValue)
        {
            case 1:
                booster.SetActive(true);
                break;
            case 2:
                health.SetActive(true);
                break;
            default:
                dummy.SetActive(true);
                break;
        }
        randomValue = Random.Range(1, 12) *30 ;
        transform.eulerAngles = new Vector3(0, randomValue, 0);
    }
}
