﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Dummy : MonoBehaviour, IDamageable
{
    public bool allowShooting, allowRotation;
    public Transform muzzle;
    public Projectile projectile;
    private float minInterval = 0.3f, maxInterval = 1;
    public float rotationSpeed = 30f;
    public float CurrentHealth { get; set; }
    bool isWaiting = false;
    IWeaponBehaviour weaponBehaviour;
    void Awake() { weaponBehaviour = new AssaultRifle(); }
    void OnEnable() { isWaiting = false; }
    IEnumerator WaitForShootingInterval()
    {
        float shootingInterval = Random.Range(minInterval, maxInterval);
        yield return new WaitForSeconds(shootingInterval);
        isWaiting = false;
    }
    public bool Damage(float damage, Vector3 location)
    {
        gameObject.SetActive(false);
        return true;
    }
    public void ResetDummy()
    {
        gameObject.SetActive(true);
    }
    private void Update()
    {
        if(allowRotation)
            transform.Rotate(new Vector3(0, Time.deltaTime * rotationSpeed, 0));

        if (allowShooting && !isWaiting)
        {
            weaponBehaviour.Shoot(projectile, muzzle);
            isWaiting = true;
            StartCoroutine(WaitForShootingInterval());
        }
    }
}
