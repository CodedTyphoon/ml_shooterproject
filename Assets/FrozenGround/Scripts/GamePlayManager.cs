﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GamePlayManager : MonoBehaviour
{
    public List<AIMind> aIMinds;
    public Transform zone;
    public float[] zoneInteval, zoneWaitTime, zoneSpeed;
    public float timer { get; private set; }
    Vector3 scaleDelta = new Vector3(0.01f, 0.01f, 0.01f);
    public int currentInterval = 0;
    public List<Transform> agentSpawnLocations = new List<Transform>();
    public List<CollectionTrainner> trainers;
    private LinkedList<IDamageable> toDamageList = new LinkedList<IDamageable>();
    public List<AIMind> AliveList = new List<AIMind>();
    public bool onePlayer, savePerformance;
    public int maxGames = 0, gameCount =0;
    public LayerMask layerMask;
    private ShuffleData shuffleData;
    // Start is called before the first frame update
    void Start()
    {
        shuffleData = new ShuffleData(aIMinds.Count);
        if (onePlayer)
        {
            AIMind aIZero = aIMinds[0];
            aIMinds.Clear();
            aIMinds.Add(aIZero);
        }
        for (int i = 0; i < aIMinds.Count; i++)
        {
            AliveList.Add(aIMinds[i]);
        }
        //StartCoroutine(InitialDelay());
        timer = 0;
    }

    // Update is called once per frame
    void Update()
    {
        timer += Time.deltaTime;
        if (currentInterval < zoneInteval.Length
                && timer > zoneWaitTime[currentInterval])
        {
            currentInterval += 1;
            StartCoroutine("ShrinkZone");
        }

        if (!onePlayer && CheckForWinner())
        {
            ResetGame();
            return;
        }
        else if (onePlayer && CheckForDead())
        {
            ResetGame();
            return;
        }
        var hits = Physics.SphereCastAll(zone.position, zone.localScale.x, Vector3.up, 0.1f, layerMask, QueryTriggerInteraction.Collide);
        //Gizmos.DrawSphere(zone.position, zone.localScale.x);
        foreach (var AI in AliveList)
        {
            bool contains = false;
            for (int i = 0; i < hits.Length; i++)
            {
                if (AI == hits[i].collider.GetComponent<AIMind>()) contains = true;
            }
            if(!contains)
                AI.Damage(10 * Time.deltaTime, Vector3.zero);
        }
    }
    public List<GameObject> spawnedItems;
    public void ResetGame()
    {
        StopCoroutine("ShrinkZone");
        timer = 0;
        currentInterval = 0;
        zone.localScale = Vector3.one*100;
        if (!onePlayer && AliveList.Count == 1)
        {
            AliveList[0].performanceStatistics.wins += 1;
            AliveList[0].AddReward(10);
            AliveList[0].EndEpisode();
            AliveList[0].GetComponent<CharacterController>().enabled = false;
            //AliveList[0].gameObject.SetActive(false);
        }
        int [] indexArray = shuffleData.GetNextShuffledArray();
        if (!onePlayer)
        {
            for (int i = 0; i < aIMinds.Count; i++)
            {
                aIMinds[i].transform.position = agentSpawnLocations[indexArray[i]].position;
                aIMinds[i].transform.rotation = agentSpawnLocations[indexArray[i]].rotation;
                //aIMinds[i].transform.position = agentSpawnLocations[i].position;
                //aIMinds[i].transform.rotation = agentSpawnLocations[i].rotation;
                aIMinds[i].Revive();
            }
        }
        else { aIMinds[0].Revive(); }

        AliveList.Clear();
        for (int i = 0; i < aIMinds.Count; i++)
        {
            AliveList.Add(aIMinds[i]);
        }
        for (int i = 0; i < spawnedItems.Count; i++)
        {
            spawnedItems[i].SetActive(true);
        }
        toDamageList.Clear();
        foreach (var trainer in trainers)
        {
            trainer.ResetTrainer();
        }

        if (maxGames != 0)
        {
            gameCount += 1;
            if (gameCount >= maxGames)
            {
#if UNITY_EDITOR
                UnityEditor.EditorApplication.isPlaying = false;
#else
                Application.Quit();
#endif
            }
        }
    }
    IEnumerator ShrinkZone()
    {
        float zoneTimer = 0;
        while (zoneTimer <= zoneInteval[currentInterval - 1])
        {
            zoneTimer += Time.deltaTime;
            zone.localScale -= Vector3.one *Time.deltaTime* zoneSpeed[currentInterval-1];
            if(zone.localScale.x<=0 && zone.localScale.y<=0 && zone.localScale.z <= 0)
            {
                zone.localScale = Vector3.zero;
                break;
            }
            yield return null;
        }
    }
    IEnumerator InitialDelay()
    {
        yield return new WaitForSeconds(5f);
        for (int i = 0; i < aIMinds.Count; i++)
        {
            aIMinds[i].gameObject.SetActive(true);
        }
    }

    //private void OnTriggerEnter(Collider other)
    //{
    //    var AI = other.GetComponent<IDamageable>();
    //    if (AI!=null)
    //    {
    //        if (toDamageList.Contains(AI)) { toDamageList.Remove(AI); }
    //    }
    //}
    //private void OnTriggerExit(Collider other)
    //{
    //    var AI = other.GetComponent<IDamageable>();
    //    if (AI !=null)
    //    {
    //        if (!toDamageList.Contains(AI))
    //            toDamageList.AddLast(AI);
    //    }
    //}
    public bool CheckForWinner()
    {
        if (AliveList.Count > 1)
        {
            for (int i = 0; i < AliveList.Count; i++)
            {
                if (AliveList[i].AgentDead)
                {
                    AliveList.RemoveAt(i);
                    i--;
                }
            }
        }
        if (AliveList.Count <= 1)
        {
            return true;
        }
        return false;
    }
    public bool CheckForDead()
    {

        if (AliveList.Count > 0)
        {
            if (AliveList[0].AgentDead)
            {
                AliveList.RemoveAt(0);
            }
        }
        if(AliveList.Count == 0)
        { 
            return true;
        }
        return false;
    }

    void OnDestroy()
    {
        if (savePerformance)
        {
            for (int i = 0; i < aIMinds.Count; i++)
            {
                string performance = JsonUtility.ToJson(aIMinds[i].performanceStatistics);
                string path = Application.dataPath + "/" + aIMinds[i].name + " Data.json";
                if (System.IO.File.Exists(path)) System.IO.File.Delete(path);
                System.IO.File.WriteAllText(path, performance);
            }
        }
    }
}

public class ShuffleData
{
    int[] indexArray;
    public int[] GetNextShuffledArray()
    {
        for (int i = 0; i < indexArray.Length; i++)
        {
            int rand = Random.Range(0, indexArray.Length);
            if (rand != i)
            {
                int temp = indexArray[i];
                indexArray[i] = indexArray[rand];
                indexArray[rand] = temp;
            }
        }
        return indexArray;
    }
    public ShuffleData(int length)
    {
        indexArray = new int[length];
        for (int i = 0; i < length; i++)
        {
            indexArray[i] = i;
        }
        for (int i = 0; i < length; i++)
        {
            int rand = Random.Range(0, length);
            if (rand != i)
            {
                int temp = indexArray[i];
                indexArray[i] = indexArray[rand];
                indexArray[rand] = temp;
            }
        }
    }
}