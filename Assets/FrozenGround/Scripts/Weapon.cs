﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum WeaponType{ Pistol, AssaultRifle, SniperRifle, ShotGun}
public class Weapon : MonoBehaviour, ICollectible
{
    public Transform muzzle;
    public Projectile projectile;
    public GameObject damageVFX;
    public float shootingInterval = 0.3f, reloadTime =  1;
    public int ammoCapacity = 10, ammoContained;
    public CollectibleType CollectibleType { get { return CollectibleType.Weapon; } }
    public WeaponType weaponType;
    public System.Action<Weapon> OnAmmoEmpty;
    public System.Action<bool> OnDamageReward;
    IWeaponBehaviour weaponBehaviour;
    bool isWaiting = false, isReloading;
    public void Initialize()
    {
        weaponBehaviour = new AssaultRifle();
        ammoContained = ammoCapacity;
    }
    public void Shoot()
    {
        if (!isWaiting && ammoContained > 0)
        {
            var bullet = weaponBehaviour.Shoot(projectile, muzzle);
            bullet.OnDamage = null;
            bullet.OnDamage += ApplyDamageReward;
            ammoContained -= 1;
            isWaiting = true;
            StartCoroutine(WaitForShootingInterval());
        }
        else if (ammoContained == 0)
        {
            OnAmmoEmpty?.Invoke(this);
        }
    }

    IEnumerator WaitForShootingInterval()
    {
        yield return new WaitForSeconds(shootingInterval);
        isWaiting = false;
    }
    public int Collect()
    {
        return 1;
    }
    /// <summary>
    /// returns remaining ammo on addition and reloads gun
    /// </summary>
    /// <param name="value"></param>
    /// <returns></returns>
    public int Reload(int value)
    {
        if (!isReloading)
        {
            isWaiting = true;
            isReloading = true;
            StartCoroutine(WaitForReloadingInterval());
            if (ammoContained < ammoCapacity)
            {
                int ammoDiff = ammoCapacity - ammoContained;
                if (ammoDiff > value)
                {
                    ammoContained += value;
                    return 0;
                }
                else
                {
                    int remaining = value - ammoDiff;
                    ammoContained = ammoCapacity;
                    return remaining;
                }
            }            
        }
        return value;
    }
    IEnumerator WaitForReloadingInterval()
    {
        yield return new WaitForSeconds(reloadTime);
        isReloading = false;
        isWaiting = false;
    }
    public void ApplyDamageReward(bool dead)
    {
        OnDamageReward?.Invoke(dead);
    }
    private void OnEnable()
    {
        isReloading = false;
        isWaiting = false;
    }
}

public interface IWeaponBehaviour
{
    Projectile Shoot(Projectile projectile, Transform muzzle);
}
public class AssaultRifle : IWeaponBehaviour
{
    public virtual Projectile Shoot(Projectile projectile, Transform muzzle)
    {
        Projectile bullet = PoolHandler.instance.GetObject(projectile) as Projectile;
        bullet.transform.MatchTransform(muzzle);
        bullet.Initialize();
        bullet.gameObject.SetActive(true);
        return bullet;
    }
}