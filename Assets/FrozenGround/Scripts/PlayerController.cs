﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour,IDamageable
{
    public Transform sphere;
    public float maxHealth = 100, currentHealth;
    public int ammoCapacity = 180, ammoHeld;
    const float MAX_HEALTH = 100;
    WeaponHandler weaponHandler;
    Motor motor;
    public float CurrentHealth { get; set; }
    // Start is called before the first frame update
    void Start()
    {
        MouseInputHandler.UpdateMousePosition += UpdateRotation;
        MouseInputHandler.OnMouseLeftClick += Shoot;
        MouseInputHandler.OnMouseRightClick += GunChange;
        maxHealth = MAX_HEALTH;
        currentHealth = maxHealth;
        motor = GetComponent<Motor>();
        weaponHandler = GetComponent<WeaponHandler>();
        weaponHandler.OnReload += Reload;
    }
    private void Update()
    {
        float h, v;
        h = Input.GetAxis("Horizontal");
        v = Input.GetAxis("Vertical");
        motor.Move(new Vector3(h, 0, v));
    }

    void UpdateRotation(Vector3 mousePos)
    {
        //sphere.position = mousePos;
        Vector3 toMouse = Vector3.ProjectOnPlane(mousePos- transform.position, Vector3.up);
        transform.rotation = Quaternion.LookRotation(toMouse, Vector3.up);
    }
    void Shoot()
    {
        weaponHandler.Shoot();
    }
    void GunChange()
    {

    }

    public void AddAmmo(int ammoCount)
    {
        if (ammoHeld <= ammoCapacity)
        {
            ammoHeld += ammoCapacity;
            ammoHeld = Mathf.Clamp(ammoHeld, 0, ammoCapacity);
        }
    }
    public void AddHealth( float healthToAdd)
    {
        if (currentHealth < maxHealth)
        {
            currentHealth += healthToAdd;
            currentHealth = Mathf.Clamp(currentHealth, 0, maxHealth);
        }
    }
    public void AddBooster( float boosterValve)
    {
        currentHealth += currentHealth * (maxHealth + boosterValve) / maxHealth;
        maxHealth += boosterValve;
        currentHealth = Mathf.Clamp(currentHealth, 0, maxHealth);
    }
    public bool Damage(float damage, Vector3 location)
    {
        currentHealth -= damage;
        if (currentHealth <= 0) Death();
        return currentHealth <= 0;
    }
    public void AttachWeapon(Weapon weapon)
    {
        weaponHandler.AttachWeapon(weapon);
    }
    void Death()
    {

    }

    public void Reload(Weapon weapon)
    {
        if (ammoHeld > 0)
        {
            ammoHeld = weapon.Reload(ammoHeld);
        }
    }
    private void OnDestroy()
    {
        MouseInputHandler.UpdateMousePosition -= UpdateRotation;
        MouseInputHandler.OnMouseLeftClick -= Shoot;
        MouseInputHandler.OnMouseRightClick -= GunChange;
    }
    private void OnTriggerEnter(Collider other)
    {
        ICollectible collectible = other.GetComponent<ICollectible>();
        if (collectible != null)
        {
            switch (collectible.CollectibleType)
            {
                case CollectibleType.Ammo:
                    AddAmmo(collectible.Collect());
                    other.GetComponent<Recycleable>().Deactivate();

                    break;
                case CollectibleType.Health:
                    AddHealth(collectible.Collect());
                    other.GetComponent<Recycleable>().Deactivate();

                    break;
                case CollectibleType.Booster:
                    AddBooster(collectible.Collect());
                    other.GetComponent<Recycleable>().Deactivate();

                    break;
                case CollectibleType.Weapon:
                    AttachWeapon((Weapon)collectible);
                    break;
                default:
                    break;
            }
        }
    }
}