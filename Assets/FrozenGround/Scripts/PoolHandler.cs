﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PoolHandler : MonoBehaviour
{
    public List<PoolList> pools;
    private Dictionary<RecycleableType, PoolList> poolListValuePair = new Dictionary<RecycleableType, PoolList>();
    public static PoolHandler instance;
    private void Awake()
    {
        Initialize();
    }
    public void Initialize()
    {
        for (int i = 0; i < pools.Count; i++)
        {
            pools[i].Initialize();
            poolListValuePair.Add(pools[i].RecycleableType, pools[i]); //=> One poolList can only contain one class
        }

        instance = this;
    }
    private void OnValidate()
    {
        foreach (var poolList in pools)
        {
            poolList.ValidatePools();
        }
    }
    public Recycleable GetObject(Recycleable prefab)
    {
        var pool = poolListValuePair[prefab.RecycleableType];
        return pool.GetObject(prefab);
    }
}
[System.Serializable]
public class PoolList
{
    public string poolListID;
    public RecycleableType RecycleableType;
    public List<Pool> poolList;
    private Dictionary<Recycleable, Pool> poolDictionary = new Dictionary<Recycleable, Pool>();
    public void Initialize()
    {
        for (int i = 0; i < poolList.Count; i++)
        {
            poolDictionary.Add(poolList[i].prefab, poolList[i]);
        }
    }
    public void ValidatePools()
    {
        foreach (var pool in poolList)
        {
            if (pool.prefab.RecycleableType != RecycleableType)
            {
                Debug.LogError("Invalid pool found, Hence removing "+ pool.poolID);
                poolList.Remove(pool);
            }
        }
    }
    public Recycleable GetObject(Recycleable recycleable)
    {
        return poolDictionary[recycleable].GetObject();
    }
}

[System.Serializable]
public class Pool
{
    public string poolID;
    public Queue<Recycleable> poolObjects = new Queue<Recycleable>();
    public Recycleable prefab;
    public Transform defaultParent;
    public Recycleable GetObject()
    {
        if (poolObjects.Count > 0)
        {
            return poolObjects.Dequeue();
        }
        else
        {
            Recycleable poolObject = GameObject.Instantiate(prefab);
            poolObject.OnRecycle += InjectObject;
            return poolObject;
        }
    }
    private void InjectObject(Recycleable poolObject)
    {
        poolObject.transform.parent = defaultParent;
        poolObjects.Enqueue(poolObject);
    }
}