﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Motor : MonoBehaviour
{
    public float speed = 5f;
    CharacterController controller;
    
    // Start is called before the first frame update
    void Start()
    {
        controller = GetComponent<CharacterController>();
    }

    public void Move(Vector3 motionDelta)
    {
        controller.Move((speed*Time.deltaTime) * motionDelta);
    }
}
