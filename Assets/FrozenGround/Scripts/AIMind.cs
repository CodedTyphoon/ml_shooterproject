﻿
using Unity.MLAgents;
using Unity.MLAgents.Policies;
using Unity.MLAgents.Sensors;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AIMind : Agent, IDamageable
{
    public bool controllable;
    public bool AgentDead
    {
        get;
        private set;
    }
    public Transform center;
    public Animator animator;
    public float maxHealth = 100;
    public float CurrentHealth{ get; set; }
    public int ammoCapacity = 180, ammoHeld;
    public LayerMask layerMask;
    public GamePlayManager playManager;

    public bool randomSpawn = false;
    public bool disableMelee = false;
    public bool scaleShootingReward = true;
    public PerformanceStatistics performanceStatistics =  new PerformanceStatistics();
    WeaponHandler weaponHandler;
    private Rigidbody m_AgentRb;
    Motor motor;
    CharacterController characterController;
    BehaviorParameters parameters;
    const float MAX_HEALTH = 100;
    private Vector3 defaultPosition, defaultRotation;

    // Start is called before the first frame update
    void Start()
    {
        if (controllable)
        {
            MouseInputHandler.UpdateMousePosition += UpdateRotation;
            //MouseInputHandler.OnMouseLeftClick += Shoot;
            //MouseInputHandler.OnMouseRightClick += GunChange;
        }
    }
    private void Update()
    {
        performanceStatistics.duration += Time.deltaTime;
        //if (controllable)
        //{
        //    float h, v;
        //    h = Input.GetAxis("Horizontal");
        //    v = Input.GetAxis("Vertical");
        //    motor.Move(new Vector3(h, 0, v));
        //}

    }
    private float angle;
    void UpdateRotation(Vector3 mousePos)
    {
        //sphere.position = mousePos;
        Vector3 toMouse = Vector3.ProjectOnPlane(mousePos - transform.position, Vector3.up);
        //transform.rotation = Quaternion.LookRotation(toMouse, Vector3.up);
        angle = Vector3.SignedAngle(transform.forward, toMouse, Vector3.up);
    }
    void GunChange()
    {

    }

    public void AddAmmo(int ammoCount)
    {
        if (ammoHeld <= ammoCapacity)
        {
            ammoHeld += ammoCapacity;
            ammoHeld = Mathf.Clamp(ammoHeld, 0, ammoCapacity);
            AddReward(0.5f);
        }
    }
    public void AddHealth(float healthToAdd)
    {
        if (CurrentHealth < maxHealth)
        {
            CurrentHealth += healthToAdd;
            CurrentHealth = Mathf.Clamp(CurrentHealth, 0, maxHealth);
            AddReward(0.25f);
        }
    }
    public void AddBooster(float boosterValve)
    {
        CurrentHealth += CurrentHealth * (maxHealth + boosterValve) / maxHealth;
        maxHealth += boosterValve;
        CurrentHealth = Mathf.Clamp(CurrentHealth, 0, maxHealth);
        AddReward(0.5f);
    }
    public bool Damage(float damage, Vector3 location)
    {
        bool alreadyDead = AgentDead;
        CurrentHealth -= damage;
        AddReward(-damage/10);
        if (CurrentHealth <= 0 && !AgentDead) Death();
        return !alreadyDead && AgentDead;
    }
    public void AttachWeapon(Weapon weapon)
    {
        weaponHandler.AttachWeapon(weapon);
    }
    void Death()
    {
        AgentDead = true;
        AddReward(-10f);
        EndEpisode();
        characterController.enabled = false;
        //enabled = false;
        collider.enabled = false;
        animator.gameObject.SetActive(false);
        if(ControlRequester)
            requester.DeRegister();
        else
            gameObject.SetActive(false);
    }

    public void Reload(Weapon weapon)
    {
        if (ammoHeld > 0)
        {
            ammoHeld = weapon.Reload(ammoHeld);
        }
    }
    public void AddDamageReward(bool dead)
    {
        if (dead)
        {
            performanceStatistics.shotkills += 1;
            AddReward(1f);
        }
        AddReward(1f);
        performanceStatistics.damage += 5;
    }
    private void OnDestroy()
    {
        if (controllable)
        {
            MouseInputHandler.UpdateMousePosition -= UpdateRotation;
            //MouseInputHandler.OnMouseLeftClick -= Shoot;
            //MouseInputHandler.OnMouseRightClick -= GunChange;
        }
    }
    private void OnTriggerEnter(Collider other)
    {
        ICollectible collectible = other.GetComponent<ICollectible>();
        if (collectible != null)
        {
            performanceStatistics.items += 1;
            AddReward(0.5f);
            switch (collectible.CollectibleType)
            {
                case CollectibleType.Ammo:
                    AddAmmo(collectible.Collect());
                    other.GetComponent<Recycleable>().Deactivate();

                    break;
                case CollectibleType.Health:
                    AddHealth(collectible.Collect());
                    other.GetComponent<Recycleable>().Deactivate();

                    break;
                case CollectibleType.Booster:
                    AddBooster(collectible.Collect());
                    other.GetComponent<Recycleable>().Deactivate();

                    break;
                case CollectibleType.Weapon:
                    AttachWeapon((Weapon)collectible);
                    break;
                default:
                    break;
            }
        }
    }
    Collider collider;
    DecisionRequester requester;
    public override void Initialize()
    {
        base.Initialize();
        AgentDead = false;

        defaultPosition = transform.position;
        defaultRotation = transform.eulerAngles;
        maxHealth = MAX_HEALTH;
        CurrentHealth = maxHealth;

        m_AgentRb = GetComponent<Rigidbody>();
        motor = GetComponent<Motor>();
        weaponHandler = GetComponent<WeaponHandler>();
        characterController = GetComponent<CharacterController>();
        collider = GetComponent<Collider>();
        requester = GetComponent<DecisionRequester>();

        if(weaponHandler.OnReload == null)
            weaponHandler.OnReload += Reload;
        if(weaponHandler.OnDamageReward == null || scaleShootingReward)
            weaponHandler.OnDamageReward += AddDamageReward;
    }
    public bool useRotControl = false;
    public override void Heuristic(float[] actionsOut)
    {
        for (int i = 0; i < 4; i++)
        {
            actionsOut[i] = 0;
        }
        if (Input.GetKey(KeyCode.D))
        {
            actionsOut[0] = 2f;
        }
        if (Input.GetKey(KeyCode.W))
        {
            actionsOut[1] = 2f;
        }
        if (Input.GetKey(KeyCode.A))
        {
            actionsOut[0] = 1f;
        }
        if (Input.GetKey(KeyCode.S))
        {
            actionsOut[1] = 1f;
        }
        //if (Input.GetKey(KeyCode.LeftArrow))
        //{
        //    actionsOut[2] = 1f;
        //}
        //if (Input.GetKey(KeyCode.RightArrow))
        //{
        //    actionsOut[2] = 2f;
        //}
        //actionsOut[3] = Input.GetKey(KeyCode.UpArrow) ? 1.0f : 0.0f;
        //Debug.Log(angle);
        var magAngle = Mathf.Abs(angle)/15;
        if      (magAngle < 2) { actionsOut[4] = 1; }
        else if (magAngle < 4) { actionsOut[4] = 2; }
        else if (magAngle < 8) { actionsOut[4] = 3; }
        else if (magAngle > 8) { actionsOut[4] = 4; }

        if (angle < -5f)
        {
            actionsOut[2] = 1f;
        }
        if (angle > 5f)
        {
            actionsOut[2] = 2f;
        }
        actionsOut[3] = Input.GetMouseButton(0) ? 1.0f : 0.0f;
        if(actionsOut[3]==0)
            actionsOut[3] = Input.GetMouseButton(1) ? 2.0f : 0.0f;
        //Debug.Log("Called");
    }
    public override void OnEpisodeBegin()
    {
        m_AgentRb.velocity = Vector3.zero;
        //if (!randomSpawn)
        //{
        //    transform.position = defaultPosition;
        //}
        //else
        //{
        //    transform.position = playManager.agentSpawnLocations[UnityEngine.Random.Range(0, playManager.agentSpawnLocations.Count)].position;
        //}
        
        transform.eulerAngles = defaultRotation;
        maxHealth = MAX_HEALTH;
        CurrentHealth = maxHealth;
        weaponHandler.activeWeapon.ammoContained = weaponHandler.activeWeapon.ammoCapacity/2;
        enabled = true;
    }

    public void Revive()
    {
        AgentDead = false;
        characterController.enabled = true;
        collider.enabled = true;
        animator.gameObject.SetActive(true);
        enabled = true;
        gameObject.SetActive(true);
        if (ControlRequester)
            requester.Register();
    }

    Vector3 lastMoveDir;
    Vector3 lastRotateDir;
    public float rotationMultiplier = 200f;
    public int shoot;
    public bool ControlRequester;
    public void MoveAgent(float[] act)
    {
        var dirToGo = Vector3.zero;
        var rotateDir = Vector3.zero;
        var h = (int)act[0];
        var v = (int)act[1];
        var rotateDirAction = (int)act[2];
        shoot = (int)act[3];
        var rotVar = useRotControl? (int)act[4] : 2f;

        if (h == 1)
            dirToGo.x = -1;
        else if (h == 2)
            dirToGo.x = 1;

        if (v == 1)
            dirToGo.z = -1;
        else if (v == 2)
            dirToGo.z = 1;

        if (rotateDirAction == 1)
            rotateDir.y = -1f;
        else if (rotateDirAction == 2)
            rotateDir.y =  1f;

        if (shoot == 1)
            Shoot();
        else if (shoot == 2)
            Melee();
        
        switch (rotVar)
        {
            case 1:
                rotationMultiplier = 50;
                break;
            case 2:
                rotationMultiplier = 100;
                break;
            case 3:
                rotationMultiplier = 200;
                break;
            case 4:
                rotationMultiplier = 400;
                break;
            default:
                rotationMultiplier = 0;
                break;
        }  

        lastMoveDir = dirToGo;
        lastRotateDir = rotateDir;
        transform.Rotate(rotateDir, Time.fixedDeltaTime * rotationMultiplier);
        //m_AgentRb.AddForce(dirToGo * 0.5f, ForceMode.VelocityChange);
        motor.Move(dirToGo);
        animator.SetFloat("BlendRun", dirToGo.sqrMagnitude);
    }
    void Shoot()
    {
        AddReward((-weaponHandler.activeWeapon.ammoCapacity + weaponHandler.activeWeapon.ammoContained) / 20);
        if(ammoHeld + weaponHandler.activeWeapon.ammoContained > 0){
            RaycastHit hit;
            if (Physics.Raycast(transform.position, transform.forward, out hit, 15f, layerMask, QueryTriggerInteraction.Collide))
            {
                if (hit.collider.gameObject.CompareTag("agent") && hit.distance > 2f)
                {
                    AddReward(1.5f);
                }
                else { AddReward(1f); }
            }
            weaponHandler.Shoot();
        }
        else { AddReward(-0.5f); }
    }
    private void Melee()
    {
        if (!disableMelee)
        {
            RaycastHit hit;
            if (Physics.SphereCast(transform.position, 1, transform.forward, out hit, 15f, layerMask, QueryTriggerInteraction.Collide))
            {
                if (hit.collider.gameObject.CompareTag("agent"))
                {
                    if (hit.distance <= 2f)
                    {
                        var damagable = hit.collider.gameObject.GetComponent<IDamageable>();
                        if (damagable.CurrentHealth <= 100 && damagable.CurrentHealth>0)
                        {
                            AddReward(1f);
                            performanceStatistics.meleeKills += 1;
                        }

                        damagable.Damage(100, hit.collider.transform.position);

                        performanceStatistics.damage += 10;
                        AddReward(1f);
                    }
                    else { AddReward(-0.25f); }
                }
            }
        }
        else { AddReward(-0.5f); }
    }

    public override void OnActionReceived(float[] vectorAction)
    {
        if (!AgentDead)
        {
            AddReward(playManager.timer * Time.deltaTime / 100);
            MoveAgent(vectorAction);
        }
        else { AddReward(-0.01f * Time.deltaTime); }
    }
    public float range = 0;
    public override void CollectObservations(VectorSensor sensor)
    {
        //sensor.AddObservation(transform.localPosition);
        sensor.AddObservation(center.position - transform.position);
        sensor.AddObservation(transform.forward);
        sensor.AddObservation(lastRotateDir.y);
        sensor.AddObservation(lastMoveDir);
        
        RaycastHit hit;
        if (Physics.Raycast(transform.position, transform.forward, out hit, 15f, layerMask, QueryTriggerInteraction.Collide))
        {
            if (hit.collider.gameObject.CompareTag("agent"))
            {
                range = 1;
                if (hit.distance <= 2f && !disableMelee)
                {
                    range = 2;
                }
            }
            else { range = 0; }
        }

        sensor.AddObservation(range);
        sensor.AddObservation(playManager.timer/100);
        sensor.AddObservation(CurrentHealth/100);
        sensor.AddObservation((ammoHeld + weaponHandler.activeWeapon.ammoContained) / 100);
        //sensor.AddObservation(maxHealth/100);
        //sensor.AddObservation(ammoCapacity/100);
    }
    
}
[System.Serializable]
public class PerformanceStatistics
{
    public float duration;
    public int  damage, items, wins, shotkills, meleeKills;
}