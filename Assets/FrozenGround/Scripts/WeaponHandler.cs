﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WeaponHandler : MonoBehaviour
{
    public Weapon activeWeapon;
    public System.Action<Weapon> OnReload;
    public System.Action<bool> OnDamageReward;
    private void Awake()
    {
        if (activeWeapon)
        {
            activeWeapon.Initialize();
            activeWeapon.OnAmmoEmpty += Reload;
            activeWeapon.OnDamageReward += DamageReward;
        }
    }
    public void Shoot()
    {
        if (activeWeapon)
        {
            activeWeapon.Shoot();
        }
        else
        {
            // TODO return negative reward
        }
    }
    public void AttachWeapon(Weapon weapon)
    {
        weapon.Initialize();
        weapon.OnAmmoEmpty += Reload;
        if (activeWeapon)
        {
            DetachWeapon(activeWeapon);
            activeWeapon.transform.position += 3f*transform.forward;
        }
        else
        {  
            // TODO positive reward 
        }
        activeWeapon = weapon;
    }
    public void DetachWeapon(Weapon weapon)
    {
        weapon.OnAmmoEmpty -= Reload;
        activeWeapon.OnDamageReward -= DamageReward;
    }
    public void Reload(Weapon weapon)
    {
        OnReload?.Invoke(weapon);
    }
    public void DamageReward(bool dead)
    {
        OnDamageReward?.Invoke(dead);
    }
}
